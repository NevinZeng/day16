## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Conduct IPM to confirm the time and ownership of each story.

2. Build CI/CD environment, distinguish dev and prod environments after automatic deployment of front-end projects, and call different back-end apis respectively.  

3. Realize the home page movie list api, realize the movie details api, and coordinate with the front-end.  

4. Implement the Session list api.  

5. Add an Advice to encapsulate the returned results.  

6. Assist team development.

## R (Reflective): Please use one word to express your feelings about today's class.

   Hard.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

1. I did not build CI/CD before, but blocked the configuration of front-end environment, such as the api that always calls production after each deployment to dev environment. Later, I found that front-end NODE_ENV always produces when npm is built. The automatic deployment runs the npm build command.

2. When the backend adds Advice, both correct and incorrect information is encapsulated as a success return, which is inconsistent with services.  

3. Tests interact with each other.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- When I encounter problems, I must learn to read the official documents, and the official documents will have what I want.
